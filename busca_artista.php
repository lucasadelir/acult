<?php
include_once("includes/conexao_artista.php");
include_once("includes/geral.php");

$titulo_pagina = "Busca Personalizada Artista";

include("menu.php");
?>
    <div class="container" style="background-color: #FFFFFF; width: 60%">
        <div id="campo-pesquisa">
            <form action="busca_artista.php" method="post">
                <input class="texto-pesquisa" type="search" name="busca" placeholder="Pesquisar artista...">
                <input class="botao-pesquisa" type="submit" class="solid" value="Buscar">
            </form>
        </div>
    </div>
<?php
$artistas = pesquisaArtistas();

if (empty($artistas)) { //Se nao achar nada, lança essa mensagem
    include("views/buscas/sem-registro.html");
}else {
    include("views/artistas/lista_artistas.php");
}

include_once("views/footer/footer.html");
?>