<?php
include_once("includes/conexao_artista.php");
include_once("includes/geral.php");

$artistas = trazerVulgos();
$titulo_pagina = "Artistas";

$id_evento = $_POST['id_evento'];

include("menu.php");
?>
    <div class="container" style="background-color: #FFFFFF; width: 60%">
        <div id="campo-pesquisa">
            <form action="busca_artista.php" method="post">
                <input class="texto-pesquisa" type="search" name="busca" placeholder="Pesquisar artista...">
                <input class="botao-pesquisa" type="submit" class="solid" value="Buscar">
            </form>
        </div>
    </div>
<?php
include("views/artistas/lista_artistas_eventos.php");