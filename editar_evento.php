<?php
include("includes/conexao_evento.php");
include("includes/geral.php");

$evento = trazerIdEventos($_POST["id"]);
$eventos = trazerEvento();
$titulo_pagina = "Editar Eventos";

include("menu.php"); ?>

<meta charset="UTF-8">
<form name="dadosEventos" action="includes/conexao_evento.php" method="POST">
    <table border="1">
        <thead>
        <tr>
            <th>Nome</th>
            <th><input type="text" name="nome_evento" value="<?= $evento["nome_evento"] ?>" size="20"/></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Data</td>
            <td><input type="date" name="data_evento" value="<?= $evento["data_evento"] ?>"/></td>
        </tr>
        <tr>
            <td>Telefone para Contato</td>
            <td><input type="text" name="contato_celular" value="<?= $evento["contato_celular"] ?>"/></td>
        </tr>
        <tr>
            <td>E-mail para Contato</td>
            <td><input type="text" name="contato_email" value="<?= $evento["contato_email"] ?>"/></td>
        </tr>
        <tr>
            <td>Endereço</td>
            <td><input type="text" name="endereco" value="<?= $evento["endereco"] ?>"/></td>
        </tr>
        <tr>
            <td>Bairro</td>
            <td><input type="text" name="bairro" value="<?= $evento["bairro"] ?>"/></td>
        </tr>
        <tr>
            <td>Número</td>
            <td><input type="text" name="numero_local" value="<?= $evento["numero_local"] ?>"/></td>
        </tr>
        <tr>
            <td>Ponto de Referência</td>
            <td><input type="text" name="referencia_local" value="<?= $evento["referencia_local"] ?>"/></td>
        </tr>
        <tr>
            <td><input type="hidden" name="acao" value="editar" />
                <input type="hidden" name="id" value="<?= $evento["id"] ?>" />
            </td>
            <td><input type="submit" value="Enviar" name="Enviar" /></td>
        </tr>



        </tbody>
    </table>

    <table border="3">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Telefone</th>
            <th>E-mail</th>
            <th>Rua</th>
            <th>Bairro</th>
            <th>Número</th>
            <th>Referência</th>
            <th>Data</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($eventos as $evento) { ?>

            <tr>
                <td><?= $evento["nome_evento"] ?></td>
                <td><?= $evento["contato_celular"] ?></td>
                <td><?= $evento["contato_email"] ?></td>
                <td><?= $evento["endereco"] ?></td>
                <td><?= $evento["bairro"] ?></td>
                <td><?= $evento["numero_local"] ?></td>
                <td><?= $evento["referencia_local"] ?></td>
                <td><?= formatoData2($evento["data_evento"]) ?></td>
            </tr>

        <?php }
        ?>
    </table>
    <?php

    function formatoData3($data) {
        $array = explode("-", $data);
        $novaData = $array[2] . "/" . $array[1] . "/" . $array[0];
        return $novaData;
    }
    ?>

    </tbody>


</form>
