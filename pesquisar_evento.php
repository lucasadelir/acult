<?php
include("includes/conexao_evento.php");
include("includes/geral.php");

$eventos = trazerEvento();
$titulo_pagina = "Eventos";

include("menu.php");
?>
<div class="container" style="background-color: #FFFFFF; width: 60%">
    <div id="campo-pesquisa">
        <form action="busca_artista.php" method="post">
            <input class="texto-pesquisa" type="search" name="busca" placeholder="Pesquisar evento...">
            <input class="botao-pesquisa" type="submit" class="solid" value="Buscar">
        </form>
    </div>
</div>
<?php
include("views/eventos/lista_eventos.php");

include_once("views/footer/footer.html");
?>
