<?php
include("includes/conexao_artista.php");
include("includes/geral.php");

$vulgos = trazerVulgos();
$vulgo = trazerIdVulgo($_POST["id"]);
$titulo_pagina = "Editar Artistas";

include("menu.php"); ?>

<meta charset="UTF-8">

<div class="container" style="background-color: #FFFFFF; width: 60%">
    <div id="campo-cadastro">
        <form name="dadosParticipante" action="includes/conexao_artista.php" enctype="multipart/form-data"
              method="POST">

            <div id="texto-inserir">
                <span class="texto-cadastro">Informação do artista</span>
            </div>
            <img src="imagens/clear.png" class="clear-page"/>
            <!-- DADOS DE INFORMAÇÃO DO ARTISTA -->
            <div class="form-group">
                <label for="exampleInputEmail1">Nome do Artista</label>
                <input type="text" name="nome_artista" value="<?= $vulgo["nome_artista"] ?>" class="form-control" placeholder="Nome do Artista">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Data de Nascimento</label>
                <input type="date" name="data_nascimento" value="<?= $vulgo["data_nascimento"] ?>" class="form-control" placeholder="Data de Nascimento">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Nome Artistico</label>
                <input type="text" name="nome_artistico" value="<?= $vulgo["nome_artistico"] ?>" class="form-control" placeholder="Nome Artistico">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Localização</label>
                <input type="text" name="localizacao_artista" value="<?= $vulgo["localizacao_artista"] ?>" class="form-control" placeholder="Cidade, Estado, etc...">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Área de Atuação</label>
                <select class="form-control" id="atuacao_artista" name="atuacao_artista">
                    <option value="'Música'">Música</option>
                    <option value="'Artes Visuais'">Artes Visuais</option>
                    <option value="'Dança'">Dança</option>
                    <option value="'Poesia'">Poesia</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Gênero Artistico</label>
                <input type="text" name="genero_artista" value="<?= $vulgo["genero_artista"] ?>" class="form-control" placeholder="Genero Artistico (Ex.: Pop, Rap, Sertanejo...)">
            </div>

            <!-- MIDIAS DO ARTISTA -->
            <div id="texto-inserir">
                <span class="texto-cadastro">Mídias do artista</span>
            </div>
            <img src="imagens/clear.png" class="clear-page"/>
            <div class="form-group">
                <label for="exampleInputEmail1">Canal de Youtube</label>
                <input type="text" name="youtube_artista" value="<?= $vulgo["youtube_artista"] ?>" class="form-control" placeholder="Canal de Youtube">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Vídeo Preferido</label>
                <input type="text" name="video_artista" value="<?= $vulgo["video_artista"] ?>" class="form-control" placeholder="Vídeo Preferido">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Usuário do Instagram</label>
                <input type="text" name="instagram_artista" value="<?= $vulgo["instagram_artista"] ?>" class="form-control" placeholder="Usuário do Instagram">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Usuário Twitter</label>
                <input type="text" name="twitter_artista" value="<?= $vulgo["twitter_artista"] ?>" class="form-control" placeholder="Usuário Twitter">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Link Spotify</label>
                <input type="text" name="spotify_artista" value="<?= $vulgo["spotify_artista"] ?>" class="form-control" placeholder="Link Spotify">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Link Soundcloud</label>
                <input type="text" name="soundcloud_artista" value="<?= $vulgo["soundcloud_artista"] ?>" class="form-control" placeholder="Link Soundcloud">
            </div>

            <!-- DADOS DE CONTATO DO ARTISTA -->
            <div id="texto-inserir">
                <span class="texto-cadastro">Contatos do artista</span>
            </div>
            <img src="imagens/clear.png" class="clear-page"/>
            <div class="form-group">
                <label for="exampleInputEmail1">Telefone</label>
                <input type="text" name="contato_celular" value="<?= $vulgo["contato_celular"] ?>" maxlength="15" class="form-control" placeholder="Telefone para contato">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Telefone 2</label>
                <input type="text" name="contato2_celular" value="<?= $vulgo["contato2_celular"] ?>" class="form-control" maxlength="15" placeholder="Segundo telefone para contato">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Endereço de email</label>
                <input type="email" name="contato_email" value="<?= $vulgo["contato_email"] ?>" class="form-control" id="exampleInputEmail1"
                       aria-describedby="emailHelp" placeholder="Seu email">
                <small id="emailHelp" class="form-text text-muted">Nunca vamos compartilhar seu email, com ninguém.
                </small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Contato de WhatsApp</label>
                <input type="text" name="whatsapp_artista" value="<?= $vulgo["whatsapp_artista"] ?>" class="form-control" maxlength="11" placeholder="Contato de WhatsApp">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Facebook Artista</label>
                <input type="text" name="facebook_artista" value="<?= $vulgo["facebook_artista"] ?>" class="form-control"  placeholder="Facebook Artista">
            </div>

            <td><input type="hidden" name="acao" value="editar"/></td>
            <td><input type="submit" value="Enviar" name="Enviar" class="btn btn-primary"/></td>
        </form>
    </div>
</div>
