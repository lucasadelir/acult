

<?php
include_once("includes/conexao_evento.php");
include_once("includes/geral.php");

$eventos = trazerEvento();
$titulo_pagina = "Criar Evento";

include_once("menu.php");


if ($_SESSION) {
    if ($_SESSION['modal'] == "1" || $_SESSION['modal'] == "2") {

        include("views/cadastro/inserir_evento.html");

    } else { ?>
        <h2>Você não possui permissão para essa tela!</h2>
    <?php }
}else{
    $login_cadastro = true;
    header('Location: logar.php');
}?>
