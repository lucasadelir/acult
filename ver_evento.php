<?php
include("includes/conexao_evento.php");
include("includes/geral.php");

$evento = trazerIdEventos($_POST["id"]);
$titulo_pagina = "Evento";
$artistas = trazerArtistaEvento();
include("menu.php"); ?>

<script type="text/javascript">
    function toggle_visibility(id) {
        var e = document.getElementById(id);
        var visivel = e.style.display == 'block';
        var menus = document.querySelectorAll('[id^=menu]');
        for (var i = 0; i < menus.length; i++) {
            menus[i].style.display = 'none';
        };
        if (visivel) e.style.display = 'none';
        else e.style.display = 'block';
    }
</script>

<div class="container" style="background-color: #FFFFFF; width: 60%">

    <div id="info-evento">
        <span class="nome-evento"><?= $evento["nome_evento"] ?></span>
        <span class="data-evento"><?= formatoData2($evento["data_evento"]) ?></span>
        <div id="banner-evento">
            <img src="includes/fotos/eventos/<?=$evento["banner_evento"]?>" />
        </div>
        <span class="localizacao-evento"><?= $evento["local_evento"] ?></span>
        <div id="botao-maps">
            <a target="_blank" href="<?= $evento["link_local"] ?>" target="_blank">
                <button class="botao-maps">Ver no mapa</button>
            </a>
        </div>
    </div>

    <div id="lista-artistas-evento">
        <div id="titulo-artistas">
            <span class="titulo-artistas">Artistas participantes</span>
        </div>
        <div id="lista-mini-artistas">
            <?php
            foreach ($artistas as $artista) {
                ?>
                <div id="mini-artista">
                    <form name="Ver Artista" action="ver_artista.php" method="POST">
                        <input type="hidden" name="id" value=<?= $artista["id"] ?> />
                        <div id="circle-mini-artista">
                            <button style="cursor: pointer">
                                <img src="includes/fotos/artistas/<?=$artista["foto_artista"]?>" />
                            </button>
                        </div>
                        <div id="dados-mini-artista">
                            <span class="nome-mini-artista"><?= $artista["nome_artistico"] ?></span>
                        </div>
                    </form>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

    <div class="submenu-evento">
        <a href="#" onclick="toggle_visibility('menu1');">
            <p>Informações</p>
        </a>
        <a href="#" onclick="toggle_visibility('menu2');">
            <p>Mídias</p>
        </a>
        <a href="#" onclick="toggle_visibility('menu3');">
            <p>Contato</p>
        </a>
    </div>

    <img src="imagens/clear.png" class="clear-page"/>

    <div id="menu1">
        <div id="descricao-evento">
            <?= $evento["descricao_evento"] ?>
        </div>
    </div>

    <div id="menu2" style="display:none;">
        <div id="youtube-midia">
            <span class="youtube-midia">YOUTUBE</span>
            <i class="fab fa-youtube fa-3x"></i>
        </div>
        <div id="youtube-midia">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $evento["video_evento"] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        <div id="botao-todos">
            <a target="_blank" target="_blank" href="<?= $evento["youtube_evento"] ?>">
                <button class="botao-mensagem">Ver todos</button>
            </a>
        </div>

        <img src="imagens/clear.png" class="clear-page"/>

        <div id="redes-evento">
            <div id="rede-evento">
                <div id="img-rede">
                    <img src="imagens/redes/instagram.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-evento">@<?= $evento["instagram_evento"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="https://www.instagram.com/<?= $evento["instagram_evento"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
            <div id="rede-evento">
                <div id="img-rede">
                    <img src="imagens/redes/spotify.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-evento"><?= $evento["nome_evento"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="<?= $evento["spotify_evento"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
            <div id="rede-evento">
                <div id="img-rede">
                    <img src="imagens/redes/twitter.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-evento">@<?= $evento["twitter_evento"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="https://www.twitter.com/<?= $evento["twitter_evento"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
        </div>

        <img src="imagens/clear.png" class="clear-page"/>

    </div>

    <div id="menu3" style="display:none;">
        <div id="email-evento">
            <span class="email-evento"><?= $evento["email_evento"] ?></span>
        </div>
        <div id="contatos-evento">
                <span class="contato-evento">
                    <i class="fas fa-phone-volume fa-2x"></i>
                    <?= $evento["contato_evento"] ?>
                </span>
            <span class="contato-evento">
                    <i class="fas fa-phone-volume fa-2x"></i>
                    <?= $evento["contato2_evento"] ?>
                </span>
        </div>

        <img src="imagens/clear.png" class="clear-page"/>

        <div id="redes-evento">
            <div id="rede-evento">
                <div id="img-rede">
                    <img src="imagens/redes/instagram.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-evento">@<?= $evento["instagram_evento"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="https://www.instagram.com/<?= $evento["instagram_evento"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
            <div id="rede-evento">
                <div id="img-rede">
                    <img src="imagens/redes/whatsapp.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-evento"><?= $evento["whatsapp_evento"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="https://wa.me/55<?= $evento["whatsapp_evento"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
            <div id="rede-evento">
                <div id="img-rede">
                    <img src="imagens/redes/facebook.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-evento"><?= $evento["nome_evento"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="<?= $evento["facebook_evento"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
            <div id="rede-evento">
                <div id="img-rede">
                    <img src="imagens/redes/twitter.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-evento">@<?= $evento["twitter_evento"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="https://www.twitter.com/<?= $evento["twitter_evento"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
        </div>

    </div>

</div>

<?php


include_once("views/footer/footer.html");