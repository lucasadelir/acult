
<div class="container" style="background-color: #FFFFFF; width: 60%">
    <table class="table table-bordered">
        <tbody>
            <?php foreach ($eventos as $evento) { ?>

                <div id="lista-evento">
                    <div id="banner-evento-lista">
                        <img src="includes/fotos/eventos/<?=$evento["banner_evento"]?>" />
                    </div>
                    <span class="nome-evento-lista"><?= $evento["nome_evento"] ?></span>
                    <div id="dados-evento">
                        <span class="data-evento-lista"><?= formatoData2($evento["data_evento"]) ?></span>
                        <span class="localizacao-evento-lista"><?= $evento["local_evento"] ?></span>
                    </div>
                    <div id="botão-lista-evento">
                        <form name="Ver Evento" action="ver_evento.php" method="POST">
                            <input type="hidden" name="id" value=<?= $evento["id"] ?> />
                            <input type="submit" value="Ver Evento" name="ver_evento" class="botão-lista-evento"/>
                        </form>
                    </div>
                </div>
                <hr>

            <?php }?>
        </tbody>
    </table>
</div>