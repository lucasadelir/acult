<div class="container" style="background-color: #FFFFFF; width: 60%">
    <table class="table table-bordered">
        <tbody>
        <?php
        foreach ($artistas as $artista) {
            ?>
            <div id="info-artista">
                <div id="circle-artista">
                    <img src="includes/fotos/artistas/<?=$artista["foto_artista"]?>" />
                </div>
                <div id="dados-artista">
                    <span class="nome-artista"><?= $artista["nome_artistico"] ?></span>
                    <span class="atuacao-artista"><?= $artista["atuacao_artista"] ?> - <?= $artista["genero_artista"] ?></span>
                    <span class="localizacao-artista"><?= $artista["localizacao_artista"] ?></span>
                    <div id="botao-mensagem">
                        <form name="Ver Artista" action="ver_artista.php" method="POST">
                            <input type="hidden" name="id" value=<?= $artista["id"] ?> />
                            <input type="submit" value="Ver Artista" name="ver_artista" class="botao-mensagem"/>
                        </form>
                    </div>
                </div>
            </div>
            <hr>
            <?php
        }?>
        </tbody>
    </table>
</div>
