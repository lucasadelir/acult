<div class="container" style="background-color: #FFFFFF; width: 60%">
    <table class="table table-bordered">
        <tbody>
        <?php
        foreach ($artistas as $artista) {
            ?>
            <div id="info-artista">
                <div id="circle-artista">
                    <img src="includes/fotos/artistas/<?=$artista["foto_artista"]?>" />
                </div>
                <div id="dados-artista">
                    <span class="nome-artista"><?= $artista["nome_artistico"] ?></span>
                    <span class="atuacao-artista"><?= $artista["atuacao_artista"] ?> - <?= $artista["genero_artista"] ?></span>
                    <span class="localizacao-artista"><?= $artista["localizacao_artista"] ?></span>
                    <div id="botao-mensagem">
                        <form name="Adicionar artista" action="includes/conexao_artista.php" method="POST">
                            <input type="hidden" name="id_artista" value=<?= $artista["id"] ?> />
                            <input type="hidden" name="id_evento" value=<?= $id_evento ?> />
                            <td><input type="hidden" name="acao" value="adicionaArtistaEvento"/></td>
                            <input type="submit" value="Adicionar artista" name="adicionar_artista" class="botao-mensagem"/>
                        </form>
                    </div>
                </div>
            </div>
            <hr>

            <?php
//            if ($_SESSION){
//                if ($artista["id_user_artista"] == $_SESSION['uid']) { ?>
<!--                    <td>-->
<!--                        <form name="Editar Artista" action="editar_artista.php" method="POST">-->
<!--                            <input type="hidden" name="id" value=--><?//= $artista['id'] ?><!-- />-->
<!--                            <input type="submit" value="Editar Artista" name="editar_artista" class="btn btn-info"/>-->
<!--                        </form>-->
<!--                    </td>-->
<!--                    <td>-->
<!--                        <form name="Excluir" action="includes/conexao_artista.php" method="POST">-->
<!--                            <input type="hidden" name="id" value=--><?//= $artista['id'] ?><!-- />-->
<!--                            <input type="hidden" name="acao" value="excluir"/>-->
<!--                            <input type="submit" value="Excluir" name="Excluir" class="btn btn-danger"/>-->
<!--                        </form>-->
<!--                    </td>-->
<!--                    --><?php
//                }?>
            <?php
        }?>
        </tbody>
    </table>
</div>
