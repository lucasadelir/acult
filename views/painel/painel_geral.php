<?php
?>
<body style="background: #FF9900">
<div class="container" style="text-align: center">
    <div id="painel-geral">
        <span class="nome-painel">Seja bem vindo <?php echo $_SESSION['nome_user'];?>!</span>

        <?php
        //PAINEL ARTISTA
        if ($_SESSION['modal'] == "0") {
            ?>
            <div id="lista-painel">
                <span class="tipo-painel">Estes são os seus Artistas</span>
                <?php
                include("includes/conexao_artista.php");
                $artistas = trazerArtistaById();
                include("views/painel/artistas_painel.php");
                ?>
            </div>
            <?php
//PAINEL EVENTO
        } elseif ($_SESSION['modal']=="1") {
            ?>
            <div id="lista-painel">
                <span class="tipo-painel">Estes são os seus Eventos</span>
                <?php
                include("includes/conexao_evento.php");
                $eventos = trazerEventoById();
                include("views/painel/eventos_painel.php");
                ?>
            </div>
            <?php
//PAINEL PREMIUM
        } elseif ($_SESSION['modal']=="2") {
            ?>
            <div id="lista-painel">
                <span class="tipo-painel">Estes são os seus Artistas:</span>
                <?php
                include("includes/conexao_artista.php");
                $artistas = trazerArtistaById();
                include("views/painel/artistas_painel.php");
                ?>
            </div>
            <div id="lista-painel">
                <span class="tipo-painel">Estes são os seus Eventos:</span>
                <?php
                include("includes/conexao_evento.php");
                $eventos = trazerEventoById();
                include("views/painel/eventos_painel.php");
                ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>
</body>
