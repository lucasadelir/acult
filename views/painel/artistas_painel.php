<div id="painel-coluna">
    <?php
    foreach ($artistas as $artista) {
        ?>
        <div id="item-coluna">
            <span class="nome-item"><?= $artista["nome_artistico"] ?></span>
            <div id="botao-painel">
                <form name="Excluir" action="includes/conexao_artista.php" method="POST">
                    <input type="hidden" name="id" value=<?= $artista['id'] ?> />
                    <input type="hidden" name="acao" value="excluir"/>
                    <input type="submit" value="" name="Excluir" class="botao-excluir"/>
                </form>
            </div>
            <div id="botao-painel">
                <form name="Editar Artista" action="editar_artista.php" method="POST">
                    <input type="hidden" name="id" value=<?= $artista['id'] ?> />
                    <input type="submit" value="" name="editar_artista" class="botao-editar"/>
                </form>
            </div>
            <div id="botao-painel">
                <form name="Ver Artista" action="ver_artista.php" method="POST">
                    <input type="hidden" name="id" value=<?= $artista["id"] ?> />
                    <input type="submit" value="" name="ver_artista" class="botao-ver"/>
                </form>
            </div>
        </div>
        <?php
    }?>
</div>
<td>
    <form name="Criar Artista" action="inserir_artista.php" method="POST">
        <input type="submit" value="Criar Artista" name="criarArtista" class="botao-criar"
               style="width=100%"/>
    </form>
</td>
