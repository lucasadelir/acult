
<?php
include_once("includes/conexao_artista.php");
include_once("includes/geral.php");

$vulgos = trazerVulgos();
$titulo_pagina = "Inserir Artista";

include_once("menu.php");

if ($_SESSION) {
    if ($_SESSION['modal'] == "0" || $_SESSION['modal'] == "2") {

        include("views/cadastro/inserir_artista.html");

    } else { ?>
        <h2>Você não possui permissão para essa tela!</h2>
    <?php }
}else{
    $login_cadastro = true;
    header('Location: logar.php');
}?>