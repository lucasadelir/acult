<?php
session_start();

function formatoData2($data)
{
    $array = explode("-", $data);
    $novaData = $array[2] . "/" . $array[1] . "/" . $array[0];
    return $novaData;
}

function usuarioLogado()
{
    if (!$_SESSION['usuario']) {
        header('Location: login.php');
        exit();
    }
}
