<?php
include_once("conexao.php");

if(isset($_POST["acao"])){
    if ($_POST["acao"]=="inserirEvento"){
        inserirEvento();
    }
    if ($_POST["acao"]=="editar"){
        editarEvento();
    }
    if ($_POST["acao"]=="excluir"){
        excluirEvento();
    }
}

function adcBannerEvento()
{
    $foto = $_FILES["banner_evento"];

    if (!empty($foto["name"])) {
        $error = array();

        if (!preg_match("/^image\/(pjpeg|jpeg|jpg|png|gif|bmp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
        }
        if (count($error) == 0) {

            // Pega extensão da imagem
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);

            // Gera um nome único para a imagem
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

            // Caminho de onde ficará a imagem
            $caminho_imagem = "fotos/eventos/" . $nome_imagem;

            // Faz o upload da imagem para seu respectivo caminho
            move_uploaded_file($foto["tmp_name"], $caminho_imagem);

            // Se houver mensagens de erro, exibe-as
            if (count($error) != 0) {
                foreach ($error as $erro) {
                    echo $erro . "<br />";
                }
            }
        }
    }
    echo($nome_imagem);

    return $nome_imagem;
}

function inserirEvento(){

    $banco = abrirBanco();
    session_start();
    $nome_imagem = adcBannerEvento();
    $sql = "INSERT INTO evento(banner_evento, nome_evento, data_evento, local_evento, link_local, descricao_evento, youtube_evento, video_evento, instagram_evento, twitter_evento, whatsapp_evento, facebook_evento, contato2_evento, email_evento, contato_evento, id_user_evento ) VALUES ( '$nome_imagem', '{$_POST["nome_evento"]}', '{$_POST["data_evento"]}', '{$_POST["local_evento"]}', '{$_POST["link_local"]}', '{$_POST["descricao_evento"]}', '{$_POST["youtube_evento"]}', '{$_POST["video_evento"]}', '{$_POST["instagram_evento"]}', '{$_POST["twitter_evento"]}', '{$_POST["whatsapp_evento"]}', '{$_POST["facebook_evento"]}', '{$_POST["contato2_evento"]}', '{$_POST["email_evento"]}', '{$_POST["contato_evento"]}', '{$_SESSION['uid']}')";
    $banco->query($sql);
    $banco->close();
    voltarPainel();
}


function trazerEvento(){
    $banco = abrirBanco();
    $sql = "SELECT * FROM evento ORDER BY nome_evento";
    $resultado = $banco->query($sql);
    $eventos = [];
    while ($row = mysqli_fetch_array($resultado)) {
        $eventos[] = $row;
    }

    return $eventos;
}
function trazerArtistaEvento(){
    $banco = abrirBanco();
    $sql = "SELECT * FROM artista AS ar JOIN evento_artistas AS ea ON ar.id = ea.id_artista WHERE ea.id_evento = '{$_POST["id"]}'";
    $resultado = $banco->query($sql);
    $eventos = [];
    while ($row = mysqli_fetch_array($resultado)) {
        $eventos[] = $row;
    }

    return $eventos;
}

function trazerEventoById(){
    $banco = abrirBanco();
    $sql = "SELECT * FROM evento WHERE id_user_evento = '{$_SESSION['uid']}' ORDER BY nome_evento";
    $resultado = $banco->query($sql);
    $eventos = [];
    while ($row = mysqli_fetch_array($resultado)) {
        $eventos[] = $row;
    }
    return $eventos;
}



function trazerIdEventos($id){
    $banco = abrirBanco();
    $sql = "SELECT * FROM evento WHERE id =".$id;
    $resultado = $banco->query($sql);
    $evento = mysqli_fetch_assoc($resultado);
    return $evento;
}

function editarEvento(){
    $banco = abrirBanco();
    $sql2 = "UPDATE evento
SET 
 nome_evento = '{$_POST["nome_evento"]}',
 data_evento = '{$_POST["data_evento"]}',
 local_evento = '{$_POST["local_evento"]}',
 link_local = '{$_POST["link_local"]}',
 descricao_evento = '{$_POST["descricao_evento"]}',
 youtube_evento = '{$_POST["youtube_evento"]}',
 video_evento = '{$_POST["video_evento"]}',
 instagram_evento = '{$_POST["instagram_evento"]}',
 twitter_evento = '{$_POST["twitter_evento"]}',
 whatsapp_evento = '{$_POST["whatsapp_evento"]}',
 facebook_evento = '{$_POST["facebook_evento"]}',
 contato2_evento = '{$_POST["contato2_evento"]}',
 email_evento = '{$_POST["email_evento"]}',
 contato_evento = '{$_POST["contato_evento"]}'
WHERE
	id = '{$_POST["id"]}'";
    $banco->query($sql2);
    $banco->close();
    voltarIndexEvento();
}

function verificaFkArtistaEvento(){

    $banco = abrirBanco();
    $id_sql_evento = $_POST["id"];
    $sqlVerificar = "SELECT * FROM evento_artistas AS ea JOIN evento AS ev ON ea.id_artista = ev.id WHERE ea.id_evento = '{$id_sql_evento}'";

    $resultado = $banco->query($sqlVerificar);
    $eventoVerificado = mysqli_fetch_assoc($resultado);
    $banco->query($sqlVerificar);

    return $eventoVerificado;
}
function apagaFkArtistaEvento(){


    $banco = abrirBanco();
    $id_sql_evento = $_POST["id"];
    $sqlVerificar = "DELETE FROM evento_artistas WHERE id_evento = '{$id_sql_evento}'";

    $banco->query($sqlVerificar);

}

function excluirEvento(){
    $banco = abrirBanco();

    $id_sql_evento = $_POST["id"];
    $sqlDeletando = "DELETE FROM evento WHERE id='$id_sql_evento'";

    $verificaFk = verificaFkArtistaEvento();

    if($verificaFk){
        apagaFkArtistaEvento();
    }

    $banco->query($sqlDeletando);
    $banco->close();

    voltarIndexEvento();
}


function voltarIndexEvento(){
    header("Location:http://localhost/acult/painel.php");
}

function voltarPainel(){
    header("Location:http://localhost/acult/painel.php");
}

function mensagemSucessoEvento(){


}