<?php
include_once("conexao.php");

if(isset($_POST["acao"])){
    if ($_POST["acao"]=="inserirArtista"){
        inserirArtista();
    }
    if ($_POST["acao"]=="editar"){
        editarVulgo();
    }
    if ($_POST["acao"]=="excluir"){
        excluirVulgo();
    }
    if ($_POST["acao"]=="adicionaArtistaEvento"){
        adicionaArtistaEvento();
    }
}

function adcImagemArtista()
{
    $foto = $_FILES["foto_artista"];


    if (!empty($foto["name"])) {
        $error = array();

        if (!preg_match("/^image\/(pjpeg|jpeg|jpg|png|gif|bmp)$/", $foto["type"])) {
            $error[1] = "Isso não é uma imagem.";
        }
        if (count($error) == 0) {

            // Pega extensão da imagem
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);

            // Gera um nome único para a imagem
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

            // Caminho de onde ficará a imagem
            $caminho_imagem = "fotos/artistas/" . $nome_imagem;

            // Faz o upload da imagem para seu respectivo caminho
            move_uploaded_file($foto["tmp_name"], $caminho_imagem);

            // Se houver mensagens de erro, exibe-as
            if (count($error) != 0) {
                foreach ($error as $erro) {
                    echo $erro . "<br />";
                }
            }
        }
    }
    return $nome_imagem;
}



function inserirArtista()
{
    $banco = abrirBanco();
    session_start();
    $nome_imagem = adcImagemArtista();
    $sql = "INSERT INTO artista ( nome_artista, contato_celular, data_nascimento, contato_email, foto_artista, nome_artistico,	genero_artista,	descricao_artista, youtube_artista,	video_artista, instagram_artista, spotify_artista, soundcloud_artista, twitter_artista,	whatsapp_artista, facebook_artista,	contato2_celular, localizacao_artista, atuacao_artista, id_user_artista)
    VALUES ( '{$_POST["nome_artista"]}', '{$_POST["contato_celular"]}', '{$_POST["data_nascimento"]}', '{$_POST["contato_email"]}', '$nome_imagem', '{$_POST["nome_artistico"]}', '{$_POST["genero_artista"]}', '{$_POST["descricao_artista"]}', '{$_POST["youtube_artista"]}', '{$_POST["video_artista"]}', '{$_POST["instagram_artista"]}', '{$_POST["spotify_artista"]}', '{$_POST["soundcloud_artista"]}', '{$_POST["twitter_artista"]}', '{$_POST["whatsapp_artista"]}', '{$_POST["facebook_artista"]}', '{$_POST["contato2_celular"]}', '{$_POST["localizacao_artista"]}', {$_POST["atuacao_artista"]}, '{$_SESSION['uid']}')";
    $banco->query($sql);
    $banco->close();

    // Se os dados forem inseridos com sucesso
    if ($sql){
        header("Location:http://localhost/acult/painel.php");
    }
}


function verificaFkEventoArtista(){

    $banco = abrirBanco();
    $id_sql_artista = $_POST["id"];
    $sqlVerificar = "SELECT * FROM evento_artistas AS ea JOIN artista AS ar ON ea.id_artista = ar.id WHERE ea.id_artista = '{$id_sql_artista}'";

    $resultado = $banco->query($sqlVerificar);
    $artistaVerificado = mysqli_fetch_assoc($resultado);
    $banco->query($sqlVerificar);

    return $artistaVerificado;
}
function apagaFkEventoArtista(){


    $banco = abrirBanco();
    $id_sql_artista = $_POST["id"];
    $sqlVerificar = "DELETE FROM evento_artistas WHERE id_artista = '{$id_sql_artista}'";

    $banco->query($sqlVerificar);

}

function excluirVulgo(){
    $banco = abrirBanco();

    $id_sql_artista = $_POST["id"];
    $sqlDeletando = "DELETE FROM artista WHERE id='$id_sql_artista'";

    $verificaFk = verificaFkEventoArtista();

    if($verificaFk){
        apagaFkEventoArtista();
    }

    $banco->query($sqlDeletando);
    $banco->close();

    voltarIndex();
}

function trazerEventoArtista(){
    $banco = abrirBanco();
    $sql = "SELECT * FROM evento AS ev JOIN evento_artistas AS ea ON ev.id = ea.id_evento WHERE ea.id_artista = '{$_POST["id"]}'";
    $resultado = $banco->query($sql);
    $eventos = [];
    while ($row = mysqli_fetch_array($resultado)) {
        $eventos[] = $row;
    }

    return $eventos;
}
function editarVulgo(){
    $banco = abrirBanco();
    $sql = "UPDATE artista
    SET nome_artista = '{$_POST["nome_artista"]}',
     contato_celular = '{$_POST["contato_celular"]}',
     data_nascimento = '{$_POST["data_nascimento"]}',
     contato_email = '{$_POST["contato_email"]}',
     nome_artistico = '{$_POST["nome_artistico"]}',
     genero_artista = '{$_POST["genero_artista"]}',
     youtube_artista = '{$_POST["youtube_artista"]}',
     video_artista = '{$_POST["video_artista"]}',
     instagram_artista = '{$_POST["instagram_artista"]}',
     spotify_artista = '{$_POST["spotify_artista"]}',
     soundcloud_artista = '{$_POST["soundcloud_artista"]}',
     twitter_artista = '{$_POST["twitter_artista"]}',
     whatsapp_artista = '{$_POST["whatsapp_artista"]}',
     facebook_artista = '{$_POST["facebook_artista"]}',
     contato2_celular = '{$_POST["contato2_celular"]}',
     localizacao_artista = '{$_POST["localizacao_artista"]}'
    WHERE
	id = '{$_POST["id"]}'";

    $banco->query($sql);
    $banco->close();
    voltarIndex();
}

function trazerVulgos(){
    $banco = abrirBanco();
    $sql3 = "SELECT * FROM artista ORDER BY nome_artista";
    $resultado = $banco->query($sql3);
    $vulgos = [];
    while ($row = mysqli_fetch_array($resultado)) {
        $vulgos[] = $row;
    }
    return $vulgos;
}

function pesquisaArtistas()
{
    $busca = $_POST['busca'];// palavra que o usuario digitou

    $banco = abrirBanco();
    $busca_query = "SELECT * FROM artista WHERE nome_artistico LIKE '%$busca%' OR nome_artista LIKE '%$busca%' OR nome_artista LIKE '%$busca%' OR genero_artista LIKE '%$busca%' OR atuacao_artista LIKE '%$busca%' OR localizacao_artista LIKE '%$busca%' ";//faz a busca com as palavras enviadas
    $resultado = $banco->query($busca_query);

    $vulgos = [];
    while ($row = mysqli_fetch_array($resultado)) {
        $vulgos[] = $row;
    }

    return $vulgos;
}

function trazerIdVulgo($id){
    $banco = abrirBanco();
    $sql6 = "SELECT * FROM artista WHERE id =".$id;
    $resultado = $banco->query($sql6);
    $vulgo = mysqli_fetch_assoc($resultado);

    return $vulgo;
}

function trazerArtistaById(){
    $banco = abrirBanco();
    $sql = "SELECT * FROM artista WHERE id_user_artista = '{$_SESSION['uid']}' ORDER BY nome_artista";
    $resultado = $banco->query($sql);
    $artistas = [];
    while ($row = mysqli_fetch_array($resultado)) {
        $artistas[] = $row;
    }
    return $artistas;
}
function adicionaArtistaEvento(){
    $banco = abrirBanco();
    $sql = "INSERT INTO evento_artistas ( id_evento, id_artista)
    VALUES ( '{$_POST["id_evento"]}', '{$_POST["id_artista"]}')";
    $banco->query($sql);
    $banco->close();

    // Se os dados forem inseridos com sucesso
    if ($sql){
        header("Location:http://localhost/acult/painel.php");
    }
}
function voltarIndex(){
    header("Location:http://localhost/acult/painel.php");
}
