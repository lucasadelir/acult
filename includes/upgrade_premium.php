<?php
include("geral.php");

$titulo_pagina = "Seja Premium";

include("../menu.php");

if ($_SESSION) {
    if ($_SESSION['modal'] == "0" || $_SESSION['modal'] == "1") {
        ?>
        <meta charset="UTF-8">
        <div class="container" style="background-color: #FFFFFF; width: 60%">
            <form name="dadosParticipante" action="includes/conexao_usuario.php" enctype="multipart/form-data"
                  method="POST">
                <td><input type="hidden" name="acao" value="upgradePremium"/></td>
                <td><input type="submit" value="Fazer Upgrade para Premium" name="Premium" class="btn btn-primary"/>
                </td>
            </form>
        </div>
    <?php } else { ?>
        <h2>Você já é usuário Premium :D</h2>
    <?php }
}else{
    header('Location: ../logar.php');
}?>