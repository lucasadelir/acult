<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 14/08/2019
 * Time: 20:54
 */

class usuarioDTO
{
    protected $usuario;
    protected $senha;
    protected $nomeUsuario;
    protected $sobrenomeUsuario;
    protected $celularUsuario;
    protected $emailUsuario;
    protected $nascUsuario;
    protected $modalUsuario;
    protected $generoUsuario;

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    /**
     * @return mixed
     */
    public function getNomeUsuario()
    {
        return $this->nomeUsuario;
    }

    /**
     * @param mixed $nomeUsuario
     */
    public function setNomeUsuario($nomeUsuario)
    {
        $this->nomeUsuario = $nomeUsuario;
    }

    /**
     * @return mixed
     */
    public function getSobrenomeUsuario()
    {
        return $this->sobrenomeUsuario;
    }

    /**
     * @param mixed $sobrenomeUsuario
     */
    public function setSobrenomeUsuario($sobrenomeUsuario)
    {
        $this->sobrenomeUsuario = $sobrenomeUsuario;
    }

    /**
     * @return mixed
     */
    public function getCelularUsuario()
    {
        return $this->celularUsuario;
    }

    /**
     * @param mixed $celularUsuario
     */
    public function setCelularUsuario($celularUsuario)
    {
        $this->celularUsuario = $celularUsuario;
    }

    /**
     * @return mixed
     */
    public function getEmailUsuario()
    {
        return $this->emailUsuario;
    }

    /**
     * @param mixed $emailUsuario
     */
    public function setEmailUsuario($emailUsuario)
    {
        $this->emailUsuario = $emailUsuario;
    }

    /**
     * @return mixed
     */
    public function getNascUsuario()
    {
        return $this->nascUsuario;
    }

    /**
     * @param mixed $nascUsuario
     */
    public function setNascUsuario($nascUsuario)
    {
        $this->nascUsuario = $nascUsuario;
    }

    /**
     * @return mixed
     */
    public function getModalUsuario()
    {
        return $this->modalUsuario;
    }

    /**
     * @param mixed $modalUsuario
     */
    public function setModalUsuario($modalUsuario)
    {
        $this->modalUsuario = $modalUsuario;
    }

    /**
     * @return mixed
     */
    public function getGeneroUsuario()
    {
        return $this->generoUsuario;
    }

    /**
     * @param mixed $generoUsuario
     */
    public function setGeneroUsuario($generoUsuario)
    {
        $this->generoUsuario = $generoUsuario;
    }


}