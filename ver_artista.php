<?php
include_once("includes/conexao_artista.php");
include_once("includes/geral.php");

$artista = trazerIdVulgo($_POST["id"]);
$titulo_pagina = "Artista";
$nome_pagina = "ver_artista";
$eventos = trazerEventoArtista();

include("menu.php"); ?>

<script type="text/javascript">
    function toggle_visibility(id) {
        var e = document.getElementById(id);
        var visivel = e.style.display == 'block';
        var menus = document.querySelectorAll('[id^=menu]');
        for (var i = 0; i < menus.length; i++) {
            menus[i].style.display = 'none';
        };
        if (visivel) e.style.display = 'none';
        else e.style.display = 'block';
    }
</script>

<div class="container" style="background-color: #FFFFFF; width: 60%">
    <div id="info-artista">
        <div id="circle-artista">
            <img src="includes/fotos/artistas/<?=$artista["foto_artista"]?>" />
        </div>
        <div id="dados-artista">
            <span class="nome-artista"><?= $artista["nome_artistico"] ?></span>
            <span class="atuacao-artista"><?= $artista["atuacao_artista"] ?> - <?= $artista["genero_artista"] ?></span>
            <span class="localizacao-artista"><?= $artista["localizacao_artista"] ?></span>
            <div id="botao-mensagem">
                <a target="_blank" href="https://wa.me/55<?= $artista["whatsapp_artista"] ?>">
                    <button class="botao-mensagem">Entrar em contato</button>
                </a>
            </div>
        </div>
    </div>

    <div class="submenu-artista">
        <a href="#" onclick="toggle_visibility('menu1');">
            <p>Informações</p>
        </a>
        <a href="#" onclick="toggle_visibility('menu2');">
            <p>Trabalhos</p>
        </a>
        <a href="#" onclick="toggle_visibility('menu3');">
            <p>Mídias</p>
        </a>
        <a href="#" onclick="toggle_visibility('menu4');">
            <p>Contato</p>
        </a>
    </div>

    <img src="imagens/clear.png" class="clear-page"/>

    <div id="menu1">
        <div id="info-nome-artista">
            <span class="info-nome-artista"><?= $artista["nome_artista"] ?></span>
            <span class="separador-info"> | </span>
            <span class="info-nome-artista"><?= $artista["localizacao_artista"] ?></span>
        </div>
        <div id="descricao-info">
            <?= $artista["descricao_artista"] ?>
        </div>
    </div>

    <div id="menu2" style="display:none;">
        <?php foreach ($eventos as $evento) { ?>
            <div id="lista-evento">
                <div id="banner-evento-lista">
                    <img src="includes/fotos/eventos/<?=$evento["banner_evento"]?>" />
                </div>
                <span class="nome-evento-lista"><?= $evento["nome_evento"] ?></span>
                <div id="dados-evento">
                    <span class="data-evento-lista"><?= formatoData2($evento["data_evento"]) ?></span>
                    <span class="localizacao-evento-lista"><?= $evento["local_evento"] ?></span>
                </div>
                <div id="botão-lista-evento">
                    <form name="Ver Evento" action="ver_evento.php" method="POST">
                        <input type="hidden" name="id" value=<?= $evento["id"] ?> />
                        <input type="submit" value="Ver Evento" name="ver_evento" class="botão-lista-evento"/>
                    </form>
                </div>
            </div>
            <hr>
        <?php }
        if(empty($eventos)): //Se nao achar nada, lança essa mensagem
        ?>
            <span class="info-nome-artista">Este artista não tem eventos marcados</span>
        <?php
        endif;
        ?>

    </div>

    <div id="menu3" style="display:none;">
        <div id="youtube-midia">
            <span class="youtube-midia">YOUTUBE</span>
            <i class="fab fa-youtube fa-3x"></i>
        </div>
        <div id="youtube-midia">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $artista["video_artista"] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        <div id="botao-todos">
            <a target="_blank" target="_blank" href="<?= $artista["youtube_artista"] ?>">
                <button class="botao-mensagem">Ver todos</button>
            </a>
        </div>

        <img src="imagens/clear.png" class="clear-page"/>

        <div id="redes-artista">
            <div id="rede-artista">
                <div id="img-rede">
                    <img src="imagens/redes/instagram.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-artista">@<?= $artista["instagram_artista"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="https://www.instagram.com/<?= $artista["instagram_artista"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
            <div id="rede-artista">
                <div id="img-rede">
                    <img src="imagens/redes/spotify.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-artista"><?= $artista["nome_artistico"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="<?= $artista["spotify_artista"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
            <div id="rede-artista">
                <div id="img-rede">
                    <img src="imagens/redes/soundcloud.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-artista"><?= $artista["nome_artistico"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="<?= $artista["soundcloud_artista"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
            <div id="rede-artista">
                <div id="img-rede">
                    <img src="imagens/redes/twitter.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-artista">@<?= $artista["twitter_artista"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="https://www.twitter.com/<?= $artista["twitter_artista"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
        </div>

        <img src="imagens/clear.png" class="clear-page"/>
    </div>

    <div id="menu4" style="display:none;">
        <div id="email-artista">
            <span class="email-artista"><?= $artista["contato_email"] ?></span>
        </div>
        <div id="contatos-artista">
                <span class="contato-artista">
                    <i class="fas fa-phone-volume fa-2x"></i>
                    <?= $artista["contato_celular"] ?>
                </span>
            <span class="contato-artista">
                    <i class="fas fa-phone-volume fa-2x"></i>
                    <?= $artista["contato2_celular"] ?>
                </span>
        </div>

        <img src="imagens/clear.png" class="clear-page"/>

        <div id="redes-artista">
            <div id="rede-artista">
                <div id="img-rede">
                    <img src="imagens/redes/instagram.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-artista">@<?= $artista["instagram_artista"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="https://www.instagram.com/<?= $artista["instagram_artista"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
            <div id="rede-artista">
                <div id="img-rede">
                    <img src="imagens/redes/whatsapp.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-artista"><?= $artista["whatsapp_artista"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="https://wa.me/55<?= $artista["whatsapp_artista"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
            <div id="rede-artista">
                <div id="img-rede">
                    <img src="imagens/redes/facebook.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-artista"><?= $artista["nome_artistico"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="<?= $artista["facebook_artista"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
            <div id="rede-artista">
                <div id="img-rede">
                    <img src="imagens/redes/twitter.png"/>
                </div>
                <div id="rede-desc">
                    <span class="rede-artista">@<?= $artista["twitter_artista"] ?></span>
                </div>
                <a target="_blank" target="_blank" href="https://www.twitter.com/<?= $artista["twitter_artista"] ?>">
                    <button class="botao-rede">Ver publicações</button>
                </a>
            </div>
        </div>

    </div>

</div>
<?php

include_once("views/footer/footer.html");