<?php
include_once("includes/geral.php");

$titulo_pagina = "Home";

include_once("menu.php"); ?>

<div class="pesquisa-capa">
    <div class="container" style="background-color: #FF9900;">
        <span class="capa-inicio">
        </span>
        <div id="banner-artista-index">
            <a href="pesquisar_artista.php">
                <img src="imagens/artista-capa.png" class="banner-artista-capa"/>
            </a>
        </div>
        <div id="barra-index">
            <div id="campo-pesquisa-index">
                <span class="texto01">Venha descobrir!</span>
                <span class="texto02">Os melhores artistas e eventos da região você encontra aqui!</span>
                <form action="busca_artista.php" method="post">
                    <input class="texto-pesquisa" type="search" name="busca" placeholder="Pesquisar artista...">
                    <input class="botao-pesquisa" type="submit" class="solid" value="Buscar">
                </form>
            </div>
            <a href="pesquisar_evento.php">
                <img src="imagens/barra-capa.png" class="barra-capa"/>
            </a>
        </div>
    </div>
</div>
<div class="container">
    <div id="marcas-capa">
        <a href="#">
            <img src="imagens/banners/capa/marca-1.png" class="banner-artista-capa"/>
        </a>
        <a href="#">
            <img src="imagens/banners/capa/marca-2.png" class="banner-artista-capa"/>
        </a>
        <a href="#">
            <img src="imagens/banners/capa/marca-3.png" class="banner-artista-capa"/>
        </a>
    </div>
</div>
<!--<div id="premium-capa">-->
<!--    <a href="pesquisar_evento.php">-->
<!--        <img src="imagens/banners/capa/seja-premium.jpg"/>-->
<!--    </a>-->
<!--</div>-->
<?php
include_once("views/footer/footer.html");