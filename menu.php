<html>
<head>
    <title>ACULT - <?php echo ($titulo_pagina)?></title>
    <?php include_once("views/header/header.html")?>
</head>

<nav class="navbar navbar-expand-lg navbar-light back_menu" style="background-color: #FF9900; height: 100px">
    <div class="container" style="width: 85%">
        <a class="navbar-brand" href="index.php">
            <img src="imagens/logos/logo.png" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <?php if(!$_SESSION):?>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="menu-navegacao">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="pesquisar_artista.php">Artistas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="pesquisar_evento.php">Eventos</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Entrar
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="login.php">Fazer Login</a>
                                <a class="dropdown-item" href="inserir_usuario.php">Fazer Cadastro</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        <?php endif;
        if($_SESSION):?>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="menu-navegacao">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="pesquisar_artista.php">Artistas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="pesquisar_evento.php">Eventos</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Olá, <?php echo $_SESSION['usuario'];?>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="painel.php">Minha Conta</a>
                                <a class="dropdown-item" href="logout.php">Sair</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        <?php endif; ?>
</nav>
