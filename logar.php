<?php
include("includes/geral.php");

$titulo_pagina = "Login";

include("menu.php");
include("views/login/login.html");

if(isset($_SESSION['senha_nao_autenticada'])):?>
    <div class="notification is-danger">
        <p>ERRO: Senha inválida.</p>
    </div>
<?php
endif;
unset($_SESSION['senha_nao_autenticada']);
?>